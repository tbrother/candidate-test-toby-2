package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class Main {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader("academy_award_actresses.csv"));
        String line;
        Scanner scanner;
        int index = 0;
        List<MovieInfo> movieList = new ArrayList<>();
        boolean isHeader = true;

        while ((line = reader.readLine()) != null) {
            if(isHeader) {
                isHeader = false;
                continue;
            }
            MovieInfo movie = new MovieInfo();
            scanner = new Scanner(line);
            scanner.useDelimiter(",");
            while (scanner.hasNext()) {
                String data = scanner.next();
                data = data.substring(1, data.length()-1);
                if (index == 0) movie.setYear(data);
                if (index == 1) movie.setActress(data);
                if (index == 2) movie.setMovie(data);
                index++;
            }
            index = 0;
            movieList.add(movie);
        }
        reader.close();
        try {
            ObjectWriter ow = new ObjectMapper().writer();
            String json = ow.writeValueAsString(movieList);
            System.out.println(json);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        }
    }
}
