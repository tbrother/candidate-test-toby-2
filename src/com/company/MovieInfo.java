package com.company;

public class MovieInfo {

    private String year;
    private String actress;
    private String movie;

    public String getYear() {
        return year;
    }
    public String getActress() {
        return actress;
    }
    public String getMovie() {
        return movie;
    }

    public void setYear(String year) {
        this.year = year;
    }
    public void setActress(String actress) {
        this.actress = actress;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

}
