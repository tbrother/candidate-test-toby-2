import { Component } from '@angular/core';
import { Http } from '@angular/http';
// import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import {Dialog} from './app.dialog.component'
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
})

export class AppComponent { 
  
  data: any;
  constructor(private http : Http){
    this.data = this.getData();
  }

  openAlert(actress:String, year:String): void {
    window.alert(actress + ' won an academy award in ' + year);
  }

  public getData(){
    return this.http.get('http://127.0.0.1:8000/academy_award_actresses.json')
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
